# ExpertCocktail

The largest list of cocktails on the web using [The Cocktail DB](https://www.thecocktaildb.com/api.php)


## Instructions

* Install [node](https://nodejs.org/) 12 or run `nvm use` with [Node Version Manager](https://github.com/nvm-sh/nvm)
* Install [npm](https://www.npmjs.com/get-npm)
* Run `npm ci` to install packages.
* Run `ng serve` for a dev server. 
* Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


