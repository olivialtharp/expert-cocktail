import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '**',
    loadChildren: () => import('./modules/cocktails/cocktails.module').then(m => m.CocktailsModule)
  },
  // TODO pages for drink by id or name
  // TODO drinks by category
  // TODO drinks by ingredient
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
