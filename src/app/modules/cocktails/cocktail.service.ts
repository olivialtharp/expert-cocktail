import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, filter } from 'rxjs/operators';

import { DrinksModel } from '../../models/drink.model';
import { FilterService } from './filter/filter.service';

@Injectable({
  providedIn: 'root'
})
export class CocktailService {

  constructor(
    private filterService: FilterService,
    private http: HttpClient,
  ) {}

  getCocktails(name: string): Observable<DrinksModel> {
    const url = `https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${name}`;

    return this.http.get<DrinksModel>(url).pipe(
      filter(response => !!response),
      catchError(error => this.errorHandler(error))
    );
  }

  private errorHandler(error: HttpErrorResponse): Observable<never> {
    return throwError(error)
  }
}
