import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilterService {
  private filter: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor() { }

  getFilter(): BehaviorSubject<string>{
    return this.filter;
  }

  setFilter(filter: string): void {
    this.filter.next(filter);
  }

  // TODO create another subject for ingredients and fork join results in cocktail service
}
