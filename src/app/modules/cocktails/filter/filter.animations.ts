import {
  trigger,
  style,
  animate,
  transition,
  state,
} from '@angular/animations';

export const FilterAnimations = [
  trigger('slide', [
    state('*', style({transform: 'translate3d(0, 0, 0)', opacity: 1 })),
    transition('void => *', [
      style({ transform: 'translate3d(0, -50%, 0)', opacity: 0 }),
      animate('100ms ease-in'),
    ]),
  ]),
];
