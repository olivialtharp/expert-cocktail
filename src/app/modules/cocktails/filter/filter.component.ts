import { Component } from '@angular/core';

import { FilterService } from './filter.service';
import { FilterAnimations } from './filter.animations';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
  animations: FilterAnimations,
})
export class FilterComponent {
  name!: string;
  hover = false;
  advanced = false;

  constructor(
    private filterService: FilterService,
  ) {}

  filterChanged(filter: string) {
    this.filterService.setFilter(filter);
  }
}
