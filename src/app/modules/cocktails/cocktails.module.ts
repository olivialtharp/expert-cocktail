import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CocktailsRoutingModule } from './cocktails-routing.module';

import { CocktailsComponent } from './cocktails.component';
import { FilterComponent } from './filter/filter.component';


@NgModule({
  declarations: [
    CocktailsComponent,
    FilterComponent
  ],
  imports: [
    CommonModule,
    CocktailsRoutingModule,
    FormsModule,
  ]
})
export class CocktailsModule { }
