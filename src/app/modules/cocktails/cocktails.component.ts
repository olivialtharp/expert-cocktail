import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';

import { CocktailService } from './cocktail.service';
import { FilterService } from './filter/filter.service';
import { DrinkModel, DrinksModel } from '../../models/drink.model';

@Component({
  selector: 'app-cocktails',
  templateUrl: './cocktails.component.html',
  styleUrls: ['./cocktails.component.scss']
})
export class CocktailsComponent implements OnInit {
  cocktails$!: Observable<DrinksModel>;
  filter = '';

  constructor(
    private cocktailService: CocktailService,
    private filterService: FilterService,
  ) {
  }

  ngOnInit(): void {
    this.filterService
      .getFilter()
      .subscribe(filter => {
        this.filter = filter ? filter : '';
        this.cocktails$ = this.cocktailService.getCocktails(this.filter);
      });

    // TODO replace nested observables
  }

  getIngredients(cocktail: any): any[] {
    const ingredients: any[] = [];
    [...Array(15)].forEach((_, i) => {
      if (cocktail['strIngredient' + i]) {
        ingredients.push(cocktail['strIngredient' + i])
      }
    });

    return ingredients;
  }

  // TODO add sorting: alphabetically?
}
